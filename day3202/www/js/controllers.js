angular.module('starter')
.controller("LoginCtrl", LoginCtrl)
.controller("HomeCtrl", HomeCtrl);

LoginCtrl.$inject = ["$ionicPlatform", "$http", "$state"];

function LoginCtrl($ionicPlatform, $http, $state){
	var vm = this;

	vm.user = {
		email: '',
		password: ''
	}

	vm.doLogin = function(){
		$http({
	      method: 'POST',
	      url: "http://demo5964607.mockable.io/login",
	      data: {"email":vm.user.email, "password":vm.user.password},
	      headers: {
	       'Content-Type': "application/json"
	      },
	    }).then(function successCallback(response){
	      if(response.status == 200){
	      	$state.go("home");
	      }
	    }, function errorCallback(response) {
	      console.log("Err: " + response)
	    });
	}


	$ionicPlatform.ready(function(){
		console.log("Entered!");
	});

}

HomeCtrl.$inject = ["$ionicPlatform", "$state"];

function HomeCtrl($ionicPlatform, $state){
	var vm = this;

	vm.message = "Welcome Home!";

	$ionicPlatform.ready(function(){
		
	});

	vm.doLogout = function(){
		$state.go("login");
	}

} 

 